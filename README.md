# Atelier Des Etoffes

### What is AtelierDe?
AtelierDe is an application to share sewing creations. The user can see the latest creations of patterns or community for find inspiration. This project is based on a personnal idea.  

---
**This Project is created with :**
* Swift 5
* IOS 16.4
* Xcode 14.3

**Team**
* FontEnd: [@Tuwleep](https://gitlab.com/Tuwleep)
* BackEnd: [@alberto.hugo05](https://gitlab.com/alberto.hugo05)

---
## Screenshot
|LoginView|HomeView|PostDetailsView|PatternListView|ShimmerView|
|--|--|--|--|--|
|![Login](./captures/LoginView.png)|![Home](./captures/HomeView.png)|![PostDetailsView](./captures/PostDetailsView.png)|![PatternListView](./captures/PatternListView.png)|![ShimmerView](./captures/PostShimmer.png)


