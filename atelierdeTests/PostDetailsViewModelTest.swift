//
//  PostDetailsViewModelTest.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 03/07/2023.
//
//  

import XCTest
@testable import atelierde

final class PostDetailsViewModelTest: XCTestCase {
    
    private var viewModel : PostDetailsViewModel!
    
    override func setUpWithError() throws {
        viewModel = PostDetailsViewModel()
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testToStateEmpty() throws {
        // GIVEN
        let expected = PostState.error()
        // WHEN
        let state = viewModel.toState(postResponse: PostResponse())
        // THEN
        XCTAssertEqual(state, expected)
    }
    
    func testToStateWithEmptyResponse() throws {
        // GIVEN
        let expected = PostState.error()
        // WHEN
        let state = viewModel.toState(postResponse: PostResponse(id: 4, patternName: "Pattern", size: PostResponse.SizeResponse(code: "code", type: "type"), images: [], fabrics: ["Fabrics", "Poupline"]
        ))
        // THEN
        XCTAssertEqual(state, expected)
    }
    
    func testToStateSuccess() throws {
        // GIVEN
        let expected = PostState.success(PostState.Post(id: 4, patternName: "Pattern", userName: "UserName", size: PostState.Size(code: "code", type: "type"), images: [], fabrics: ["Fabrics", "Poupline"]))
        // WHEN
        let state = viewModel.toState(postResponse: PostResponse(id: 4, patternName: "Pattern", userName: "UserName", size: PostResponse.SizeResponse(code: "code", type: "type"), images: [], fabrics: ["Fabrics", "Poupline"]
        ))
        // THEN
        XCTAssertEqual(state, expected)
    }
    
}
