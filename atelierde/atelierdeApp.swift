//
//  atelierdeApp.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 08/06/2023.
//
//  

import SwiftUI

@main
struct atelierdeApp: App {
    
    var user = MainRepository.shared.user
    // Créez une instance de la classe User
    
    var body: some Scene {
        WindowGroup {
            NavigationView()
                .environmentObject(user)
        }
    }
}
