//
//  PatternsState.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 12/07/2023.
//
//  

import Foundation

enum PatternsState : Equatable {
    struct PatternsListState : Equatable {
        var patterns: [PatternsElement] = []
    }
    
    case loading
    case success(PatternsListState)
    case error(String? = nil)
}

