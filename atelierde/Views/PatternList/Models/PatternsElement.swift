//
//  PatternsElement.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 12/07/2023.
//
//  

import Foundation

struct PatternsElement: Identifiable, Equatable {
    let uuid: UUID = UUID()
    let id: Int
    let name: String
    let creatorName: String
    let image: String
}
