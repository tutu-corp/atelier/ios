//
//  PatternCardView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 10/07/2023.
//
//  

import SwiftUI

struct PatternCardView: View {
    
    var image : String
    var title : String
    var creator : String
    var id: Int
    
    var body: some View {
        VStack{
            NavigationLink{
                PatternDetailsRoute(id: id)
            } label: {
                HStack {
                    
                    
                    ImagePattern
                    PatternDetailsCard
                    Spacer()
                    ImageLink
                    
                }
                .padding()
            }
        }
    }
    
    private var ImagePattern : some View {
        AsyncImage(url: URL(string: image)) { image in
            image
                .ImageStyle()
                .modifier(ImageLittleSize())
        } placeholder: {
            Color.gray
                .EmptyImageLoading()
                .modifier(ImageLittleSize())
        }
    }
    
    private var PatternDetailsCard : some View {
        VStack(alignment: .leading){
            Text(title)
                .font(.title2)
                .foregroundColor(.primary)
            Text(creator)
                .modifier(TextSecondaryStyle())
            
        }
    }
    
    private var ImageLink : some View {
        Image(systemName: "chevron.compact.right")
            .font(.title)
            .modifier(TextSecondaryStyle())
    }
}

struct PatternCardView_Previews: PreviewProvider {
    static var previews: some View {
        PatternCardView(image: "crea-bob1", title: "Le patron", creator: "Le créateur", id: 3)
    }
}
