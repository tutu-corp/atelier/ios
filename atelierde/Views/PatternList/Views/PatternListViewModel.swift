//
//  PatternListViewModel.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 10/07/2023.
//
//  

import Foundation

class PatternListViewModel: ObservableObject{
    
    @Published var state: PatternsState
    
    private let mainRepository = MainRepository.shared
    
    init(state: PatternsState = .loading) {
        self.state = state
    }
    
    func fetchPatternsListData() async throws -> [PatternListResponse] {
        return try await mainRepository.patternListData()
    }
    
    func toState(patternResponse: [PatternListResponse]) -> PatternsState {
        let patterns = patternResponse.map { pattern in
            PatternsElement(
                id:pattern.id,
                name: pattern.name,
                creatorName: pattern.creatorName,
                image: pattern.filePath)
        }
        let patternsState = PatternsState.success(PatternsState.PatternsListState(patterns: patterns))
        
        return patternsState
    }
}
