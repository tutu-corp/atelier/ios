//
//  PatternListView+loading.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 04/08/2023.
//
//  

import Foundation
import Shimmer
import SwiftUI

extension PatternListView {
    var loadingView : some View {
        ScrollView(showsIndicators: false) {
            
            let patternListLoading = PatternsState.PatternsListState(patterns: [
                PatternsElement(id: 3, name: "Froufrou", creatorName: "By bidouille", image: "crea-bob2"),
                PatternsElement(id: 3, name: "Bigornio", creatorName: "By Trumuche", image: "crea-bob1"),
                PatternsElement(id: 3, name: "Bigornio", creatorName: "By Trumuche", image: "crea-bob1"),
                PatternsElement(id: 3, name: "Bigornio", creatorName: "By Trumuche", image: "crea-bob1")
            ])
            
            titleContent
            patternsList(patternList: patternListLoading)
        }
        .padding()
        .redacted(reason: .placeholder)
        .shimmering()
    }
}
