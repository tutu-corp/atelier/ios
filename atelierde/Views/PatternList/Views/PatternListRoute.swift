//
//  PatternListRoute.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 03/08/2023.
//
//  

import SwiftUI

struct PatternListRoute: View {
    
    @ObservedObject var viewModel = PatternListViewModel()
    
    var body: some View {
        PatternListView(state: $viewModel.state)
            .task {
                await fetchData()
            }
    }
}

extension PatternListRoute {
    func fetchData() async {
        do {
            let data = try await viewModel.fetchPatternsListData()
            let state = viewModel.toState(patternResponse: data)
            
            print("Patterns : \(state)")
            
            
            try await Task.sleep(nanoseconds: MainRepository.shared.serverDelay)
            viewModel.state = state
        } catch {
            print("Error info: \(error)")
        }
        
    }
}


