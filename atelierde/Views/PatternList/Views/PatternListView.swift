//
//  PatternListView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 10/07/2023.
//
//  

import SwiftUI

struct PatternListView: View {
    
    @Binding var state : PatternsState
    
    let patternListLoading = PatternsState.PatternsListState(patterns: [
        PatternsElement(id: 3, name: "Froufrou", creatorName: "By bidouille", image: "crea-bob2"),
        PatternsElement(id: 3, name: "Bigornio", creatorName: "By Trumuche", image: "crea-bob1"),
    ])
    
    var body: some View {
        VStack {
            switch state {
            case .loading:
                loadingView
                
            case .success(let patternList):
                NavigationStack {
                    ScrollView(showsIndicators: false) {
                        
                        titleContent
                        patternsList(patternList: patternList)
                        
                    }
                    .padding(.horizontal)
                }
            case .error(let error):
                Text(error ?? "Vous etes perdu ?")
            }
        }
        
    }
    
    var titleContent: some View {
        HStack {
            Text("pattern.title")
                .modifier(TitleStyle())
            Spacer()
        }
    }
    
    func patternsList(patternList : PatternsState.PatternsListState) -> some View {
        ForEach(patternList.patterns, id: \.uuid) { pattern in
            PatternCardView(image: pattern.image, title: pattern.name, creator: pattern.creatorName, id: pattern.id)
        }
        
    }
}

struct PatternListView_Previews: PreviewProvider {
    static var previews: some View {
        
        let patternListPreview = PatternsState.PatternsListState(patterns: [
            PatternsElement(id: 2, name: "Froufrou", creatorName: "By bidouille", image: "crea-bob2"),
            PatternsElement(id: 3, name: "Bigornio", creatorName: "By Trumuche", image: "crea-bob1"),
        ])
        
        PatternListView(state: .constant(.success(patternListPreview)))
        
        PatternListView(state: .constant(.loading))
    }
}
