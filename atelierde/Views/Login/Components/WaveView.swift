//
//  WaveView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 08/06/2023.
//
//  

import SwiftUI

struct WaveView: View {
    var body: some View {
        Path { path in
            //Top left
            path.move(to: CGPoint(x: 0, y: 0))
            //Left vertical bound
            path.addLine(to: CGPoint(x: 0, y: 200))
            //Curve
            path.addCurve(to: CGPoint(x: 430, y: 200),
                          control1: CGPoint(x: 250, y: 40),
                          control2: CGPoint(x: 175, y: 400))
            //Right vertical bound
            path.addLine(to: CGPoint(x: 450, y: 0))
        }
        .fill(Color("melon"))
        .edgesIgnoringSafeArea(.top)
    }
}

struct WaveView_Previews: PreviewProvider {
    static var previews: some View {
        WaveView()
    }
}
