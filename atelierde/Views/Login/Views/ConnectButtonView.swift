//
//  ConnectButtonView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 31/07/2023.
//
//  

import SwiftUI

struct ConnectButtonView: View {
    
    let email : String = "tutucorp@gmail.com"
    
    var body: some View {
        
        Button(action: {
            Task {
                do {
                   let token = try await MainRepository.shared.login(email: email)
                    MainRepository.shared.user.setToken("Bearer " + token)
//                     Stop loader, success
                } catch {
                    print("Error info: \(error)")
                }
            }
        }, label: {
            Text("button.connect")
                .foregroundColor(Color.black)
        })
        .buttonStyle(.borderedProminent)
        .accentColor(/*@START_MENU_TOKEN@*/Color("melon")/*@END_MENU_TOKEN@*/)
        
    }
}
    
    struct ConnectButtonView_Previews: PreviewProvider {
        static var previews: some View {
            ConnectButtonView()
        }
    }
