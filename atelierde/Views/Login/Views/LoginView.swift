//
//  LoginView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 08/06/2023.
//
//  

import SwiftUI

struct LoginView: View {
    var body: some View {
        ZStack {
            WaveView()
            VStack {
                Spacer()
                ImageHeader
                Header
                Spacer()
                    .frame(height: 60)
                ConnectButtonView()
//                SignInButtonView()
                Spacer()
                    .frame(height: 60)
            }
            .padding()
        }
    }
    
    private var ImageHeader: some View {
        Image("undrawFlowers")
            .resizable()
            .scaledToFit()
            .padding()
    }
    
    private var Header: some View {
        VStack {
            Text("login.title")
                .font(.largeTitle)
                .bold()
            Divider()
                .frame(width: 200)
            Text("login.subtitle")
                .font(.subheadline)
        }
    }
}


struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
