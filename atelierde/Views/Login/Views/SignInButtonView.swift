//
//  SignInButtonView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 21/07/2023.
//
//

import SwiftUI
import AuthenticationServices

struct SignInButtonView: View {
    @EnvironmentObject var user: User
    // Récupérez l'objet User depuis l'environnement
    
    
    var body: some View {
        SignInWithAppleButton(
            onRequest: { request in
                request.requestedScopes = [.fullName, .email]
            },
            onCompletion: { result in
                //              Gérer le résultat de l'autorisation ici
                switch result {
                case .success(let authorization):
                    if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
                        //                         Récupération des données de l'utilisateur par Apple
                        
                        
                        // let email = appleIDCredential.email
                        // let fullName = appleIDCredential.fullName
                        // let jwt = String(data: appleIDCredential.identityToken!, encoding: .utf8)
                        
                        // Vérifier si l'utilisateur existe déjà dans votre système en utilisant userIdentifier ou email.
                        
                        if let email = appleIDCredential.email {
                            Task {
                                do {
                                    let token = try await MainRepository.shared.login(email: email)
                                    // Stop loader, success
                                } catch {
                                    print("Error info: \(error)")
                                }
                            }
                            
                            
                           
                            print(email)
                            
                            if let fullName = appleIDCredential.fullName {
                                print(fullName)
                            } else {
                                print("Je n'ai pas de name")
                            }
                            
                        } else {
//                            print(appleIDCredential.email)
                            print("Je n'ai pas d'email")
                        }
                        
                        
                        
                        
                        if user.isLoggedIn {
                            // L'utilisateur viens d'être créer = page pour remplir son profil
//                            NavigationLink(destination: HomeView()) { EmptyView() }
                        } else {
                            // L'utilisateur viens d'être créer = page pour remplir son profil
//                            NavigationLink(destination: HomeView()) { EmptyView() }
                        }
                    }
                case .failure(let error):
                    //                    Erreurs de connexion ici
                    print("Erreur de connexion avec Apple : \(error.localizedDescription)")
                }
            }
        )
        .frame(width: 280, height: 45)
        .signInWithAppleButtonStyle(.black)
        .padding(30)
    }
}

struct SignInButtonView_Previews: PreviewProvider {
    static var previews: some View {
        SignInButtonView()
    }
}
