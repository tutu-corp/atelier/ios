//
//  ButtonView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 15/06/2023.
//
//  

import SwiftUI

struct PrimaryStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(8)
            .background(Color("melon"))
            .foregroundColor(.primary)
            .clipShape(Capsule())
            .scaleEffect(configuration.isPressed ? 1.2 : 1)
            .animation(.easeOut(duration: 0.2), value: configuration.isPressed)
    }
}

struct ButtonView_Previews: PreviewProvider {
    static var previews: some View {
        Button() {
        } label: {
            HStack{
                Image(systemName: "pencil.tip")
                Text("Bouton")
                    .font(.caption2)
            }
        }
        .buttonStyle(PrimaryStyle())
    }
}
