//
//  PostDetailsRoute.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 20/07/2023.
//
//  

import Foundation


import SwiftUI

struct PostDetailsRoute: View {
    
    @ObservedObject var viewModel = PostDetailsViewModel()
    var id : Int
    var showCreator : Bool = true
    
    var body: some View {
        PostDetailsView(state: $viewModel.state, id: id, showCreator: showCreator)
            .task {
                await fetchData()
            }
    }
}

extension PostDetailsRoute {
    func fetchData() async {
        do {
            let data = try await viewModel.fetchPostData(id: id)
            let state = viewModel.toState(postResponse: data)
            
            try await Task.sleep(nanoseconds: MainRepository.shared.serverDelay)
            viewModel.state = state
        } catch {
            viewModel.state = .error("Error info: \(error)")
        }
        
    }
}
