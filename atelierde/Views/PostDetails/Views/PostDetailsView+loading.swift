//
//  PostDetailsView+loading.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 04/08/2023.
//
//  

import Foundation
import Shimmer
import SwiftUI

extension PostDetailsView {
    var loadingView : some View {
        VStack{
            
            let postLoading = PostState.Post(id: 4, patternName: "lalalala", user: PostState.User(id: 3, name: "lululululu"), size: PostState.Size(code: "code", type: "type"), images: [], fabrics: ["Fabrics", "Poupline"])
            
            Color.gray
                .EmptyImageLoading()
            SpacerLarge
            DetailsView(post: postLoading)
            SpacerLarge
            CardView(userCreator: postLoading.user.name, exp: postLoading.user.exp ?? 0)
                .padding()
        }
        .padding()
        .redacted(reason: .placeholder)
        .shimmering()
    }
}
