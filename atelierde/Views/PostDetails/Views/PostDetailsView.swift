//
//  PostDetailsView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 26/06/2023.
//
//  

import SwiftUI

struct PostDetailsView: View {
    
    @Binding var state : PostState
    var id : Int
    var showCreator : Bool = true
    
    var body: some View {
        VStack {
            switch state {
            case .loading:
                loadingView
                
            case .success (let post):
                ScrollView(showsIndicators: false) {
                    LazyVStack{
                        SliderView(images: post.images)
                        SpacerLarge
                        DetailsView(post: post)
                        SpacerLarge
                    }
                    
                    
                    if showCreator == true {
                        HStack {
                            Text("Réalisé par :")
                                .modifier(TextSecondaryStyle())
                            Spacer()
                        }
                        
                        NavigationLink {
                            ProfilRoute(id: post.user.id)
                        } label: {
                            CardView(userCreator: post.user.name, exp: post.user.exp ?? 0)
                                .padding()
                        }
                    }
                }
                .padding()
            case .error (let error):
                Text(error ?? "Vous etes perdu ?")
            }
        }
    }
    
    func DetailsView(post : PostState.Post) -> some View {
        VStack {
            HStack{
                Text("post.pattern")
                    .modifier(TextSecondaryStyle())
                Spacer()
                Text(post.patternName)
            }
            Divider()
            
            if let size = post.size {
                HStack{
                    Text("post.size")
                        .modifier(TextSecondaryStyle())
                    Spacer()
                    Text(size.code)
                }
            }
            
            Divider()
            
            HStack{
                Text("post.fabric")
                    .modifier(TextSecondaryStyle())
                Spacer()
                Text(post.fabrics.joined(separator: ", "))
                //                Text(post.fabrics.first ?? " ")
            }
        }
    }
    
    var SpacerLarge: some View {
        Spacer()
            .frame(height: 35)
    }
}

struct PostDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        let postPreview = PostState.Post(id: 4, patternName: "lalalala", user: PostState.User(id: 3, name: "lulululu", exp: 8), size: PostState.Size(code: "code", type: "type"), images: [], fabrics: ["Fabrics", "Poupline"])
        
        PostDetailsView(state: .constant(.success(postPreview)), id: 0)
        
        PostDetailsView(state: .constant(.loading), id: 0)
        
        PostDetailsView(state: .constant(.error()), id: 0)
    }
}


