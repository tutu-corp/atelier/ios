//
//  PostDetailsViewModel.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 27/06/2023.
//
//  

import Foundation


class PostDetailsViewModel: ObservableObject {
    
    @Published var state : PostState
    
    private let mainRepository = MainRepository.shared
    
    init(state: PostState = .loading) {
        self.state = state
    }
    
    func fetchPostData(id: Int) async throws -> PostResponse {
        return try await mainRepository.postData(id: id)
    }
    
    func toState(postResponse : PostResponse) -> PostState {
        
        guard let id = postResponse.id else {
            return .error()
        }
        
        guard let patternName = postResponse.patternName else {
            return .error()
        }
        
        let user = PostState.User(id: postResponse.user!.id!, name: postResponse.user!.name!, exp: postResponse.user!.exp)
        
        let size : PostState.Size?
        
        if let code = postResponse.size?.code, let type = postResponse.size?.type  {
            size = PostState.Size(code: code, type: type)
        } else {
            size = nil
        }

        let postState = PostState.success(
            PostState.Post(
                id: id,
                patternName: patternName,
                user: user,
                size: size,
                images: postResponse.images?.map({ image in
                    image
                }) ?? [],
                fabrics: postResponse.fabrics ?? []
            )
        )
        
        return postState
    }
}

