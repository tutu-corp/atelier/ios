//
//  PostState.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 27/06/2023.
//
//  

import Foundation

enum PostState: Equatable {
    
    struct Post: Codable, Equatable {
        var id: Int
        var patternName: String
        var user: User
        var size : Size?
        var images: [String]
        var fabrics: [String]
    }
    struct User : Codable, Equatable {
        var id: Int
        var name: String
        var exp: Int?
    }
    
    struct Size : Codable, Equatable {
        var code: String
        var type: String
    }
    
    case loading
    case success(Post)
    case error(String? = nil)
}
