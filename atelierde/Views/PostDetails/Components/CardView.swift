//
//  CardView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 26/06/2023.
//
//  

import SwiftUI

struct CardView: View {
    
    let userCreator : String
    let exp : Int
    
    var body: some View {
        HStack{
            VStack (alignment: .leading){
                
                Text(userCreator)
                
                    .font(.custom("Mangabey", size: 35))
                Text("\(exp) ans de couture")
                    .modifier(TextSecondaryStyle())
            }
            Spacer()
        }
        .modifier(CardStyle())
        
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(userCreator: "Schtroumph", exp: 7)
    }
}
