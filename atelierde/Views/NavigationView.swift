//
//  NavigationView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 15/06/2023.
//
//  

import SwiftUI

struct NavigationView: View {
    
    @EnvironmentObject var user: User
    
    var body: some View {
        
        if user.isLoggedIn {
            TabView{
                HomeViewRoute()
                    .tabItem {
                        Label("nav.menu", systemImage: "house.fill")
                    }
                
                
                HomeViewRoute()
                    .tabItem{
                        Label("nav.likes", systemImage: "lasso.and.sparkles")
                    }
                
                PatternListRoute()
                    .tabItem{
                        Label("nav.patterns", systemImage: "magazine")
                    }
                
//                ProfilView()
//                    .tabItem{
//                        Label("Profil", systemImage: "person.fill")
//                    }
                
            }
            .accentColor(Color("melon"))
        } else {
            LoginView()
                .tabItem{
                    Label("Profil", systemImage: "person.crop.circle")
                }
        }
        
        
    }
}

//struct NavigationView_Previews: PreviewProvider {
//    static var previews: some View {
//        NavigationView()
//            .environmentObject(User(token: "Blublu"))
//    }
//}
