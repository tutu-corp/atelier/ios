//
//  PostCardView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 15/06/2023.
//
//  

import SwiftUI
import Shimmer

struct PostCardView: View {
    
    var id: Int
    var image: String?
    var creatorUser: String
    
    var body: some View {
        NavigationLink {
            PostDetailsRoute(id: id)
        } label: {
            VStack {
                if let image = image {
                    AsyncImage(url: URL(string: image)) { image in
                        image
                            .ImageStyle()
                    } placeholder: {
                        Image("melonPlaceholder")
                            .ImageStyle()
                            .opacity(0.2)
                    
                    }
                } else {
                    Color.gray
                        .EmptyImageLoading()
                }
                
                postDetails
                
            }
        }
    }
    
    private var postDetails: some View {
        Group {
            HStack {
                Text(creatorUser)
                    .font(.footnote)
                    .bold()
                
                Spacer()
                Image(systemName: "heart.fill")
                    .foregroundColor(Color("melon"))
            }
            .foregroundColor(.primary)
            .padding(.horizontal)
            .padding(.vertical, -3.0)
        }
    }
}

struct PostCardView_Previews: PreviewProvider {
    static var previews: some View {
        PostCardView(id: 0, image: "crea-bob1", creatorUser: "Flanette")
    }
}

struct PostCardViewRed_Previews: PreviewProvider {
    static var previews: some View {
        PostCardView(id: 0, creatorUser: "Flanette")
    }
}

