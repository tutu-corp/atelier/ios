//
//  Post.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 20/06/2023.
//
//  

import Foundation

struct HomePost: Codable, Equatable {
    var uuid: UUID = UUID()
    let id: Int
    let image: String
    let creatorUser: String
}
