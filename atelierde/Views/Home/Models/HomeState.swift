//
//  HomeState.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 20/06/2023.
//
//  

import Foundation

enum HomeState: Equatable {
    struct HomePostList: Codable, Equatable {
        var posts: [HomePost] = []
    }
    
    case loading
    case success(HomePostList)
    case error(String? = nil)
}
