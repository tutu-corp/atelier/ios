//
//  HomeView+loading.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 03/08/2023.
//
//  

import SwiftUI
import Shimmer

extension HomeView {
    var loadingView : some View {
        ScrollView(showsIndicators: false) {
            titleContent
            postGridLoading
        }
        .padding()
        .redacted(reason: .placeholder)
        .shimmering()
    }
}

private var postGridLoading : some View {
    LazyVGrid(
        columns: [
            GridItem(.adaptive(minimum: 160)),
            GridItem(.adaptive(minimum: 160))
        ], spacing: 10) {
            PostCardView(id: 0, creatorUser: "Ici c'est le Name")
            PostCardView(id: 0, creatorUser: "Ici c'est le Name")
            PostCardView(id: 0, creatorUser: "Ici c'est le Name")
            PostCardView(id: 0, creatorUser: "Ici c'est le Name")
            PostCardView(id: 0, creatorUser: "Ici c'est le Name")
        }
}

