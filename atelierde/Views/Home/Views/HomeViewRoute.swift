//
//  HomeViewRoute.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 31/07/2023.
//
//  

import SwiftUI

struct HomeViewRoute: View {
    
    @ObservedObject var viewModel = HomeViewModel()
    
    var body: some View {
        HomeView(state: $viewModel.state)
            .task {
                await fetchData()
            }
    }
}

extension HomeViewRoute {
    func fetchData() async {
            do {
                let data = try await viewModel.fetchHomeData()
                let state = viewModel.toState(homeResponse: data)
                
                    print("Home : \(state)")
                try await Task.sleep(nanoseconds: MainRepository.shared.serverDelay)
                viewModel.state = state
            } catch {
                print("Error info: \(error)")
            }
    }
}
