//
//  HomeView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 08/06/2023.
//
//  

import SwiftUI
import Shimmer

struct HomeView: View {
    
    @Binding var state : HomeState
    
    var body: some View {
        VStack{
            
            switch state {
            case .loading:
                loadingView
                
            case .success(let postList):
                NavigationStack {
                    ScrollView(showsIndicators: false) {
                        
                        titleContent
                        postGrid(postList: postList)
                    }
                }
                .padding(.horizontal)
            case .error (let error):
                Text(error ?? "Vous etes perdu ?")
            }
        }
    }
    
    var titleContent: some View {
        HStack {
            Text("home.title")
                .modifier(TitleStyle())
            Spacer()
            
            NavigationLink {
            
            } label: {
                Image(systemName: "plus")
                    .symbolRenderingMode(.hierarchical)
                    .foregroundStyle(Color("melon"))
                    .font(Font.system(.title))
                    .accessibilityLabel("home.profil")
            }
            
            NavigationLink {
                ProfilRoute(id: nil)
            } label: {
                Image(systemName: "person.fill")
                    .symbolRenderingMode(.hierarchical)
                    .foregroundStyle(Color("melon"))
                    .font(Font.system(.title))
                    .accessibilityLabel("home.profil")
            }
        }
    }
    
    private func postGrid(postList : HomeState.HomePostList) -> some View {
        LazyVGrid(
            columns: [
                GridItem(.adaptive(minimum: 160)),
                GridItem(.adaptive(minimum: 160))
            ], spacing: 10) {
                ForEach(postList.posts, id: \.uuid) { post in
                    PostCardView(id: post.id, image: post.image, creatorUser: post.creatorUser)
                }
            }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        let postListPreview = HomeState.HomePostList(posts: [
            HomePost(id: 0, image: "crea-bob1", creatorUser: "Flanette"),
            HomePost(id: 1, image: "crea-bob2", creatorUser: "CupcakeLover")
        ])
        return HomeView(state: .constant(.success(postListPreview)))
    }
}

struct HomeViewLoading_Previews: PreviewProvider {
    static var previews: some View {
        return HomeView(state: .constant(.loading))
    }
}
