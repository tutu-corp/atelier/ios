//
//  HomeViewModels.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 19/06/2023.
//
//  

import Foundation

class HomeViewModel: ObservableObject{
    
    @Published var state: HomeState
    
    private let mainRepository = MainRepository.shared
    
    init(state: HomeState = .loading) {
        self.state = state
    }
    
    func fetchHomeData() async throws -> HomeResponse {
        return try await mainRepository.homeData()
    }
    
    func toState(homeResponse: HomeResponse) -> HomeState {
        let posts = homeResponse.creations.map { creation in
            HomePost(
                id:creation.id ,
                image: creation.images.first!,
                creatorUser: creation.user.name)
        }
        let homeState = HomeState.success(HomeState.HomePostList(posts: posts))
                
        
        return homeState
    }
}
