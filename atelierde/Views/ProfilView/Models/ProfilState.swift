//
//  ProfilState.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 14/08/2023.
//
//  

import Foundation

enum ProfilState: Equatable {
    
    struct User: Codable, Equatable {
        var name : String
        var experience : Int
        var creationsList: [ProfilPost]
    }

    case loading
    case success(User)
    case error(String? = nil)
}

