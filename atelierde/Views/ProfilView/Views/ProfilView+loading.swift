//
//  ProfilView+loading.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 18/08/2023.
//
//  

import Foundation
import Shimmer
import SwiftUI

extension ProfilView {
    var loadingView : some View {
        ScrollView(showsIndicators: false) {
            
            let profilLoading = ProfilState.User(name: "Bigorneau", experience: 7, creationsList: [
                ProfilPost(id: 4, image: "")])
            
            Text(profilLoading.name)
                .modifier(TitleStyle())
            Text("\(profilLoading.experience) année de couture")
                .modifier(TextSecondaryStyle())
            Divider()
            gridContent(postGrid: profilLoading)
            
        }
        .padding(.horizontal)
        .redacted(reason: .placeholder)
        .shimmering()
    }
}
