//
//  ProfilRoute.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 14/08/2023.
//
//  

import Foundation
import SwiftUI

struct ProfilRoute: View {
    
    @ObservedObject var viewModel = ProfilViewModel()
    var id : Int?
    
    var body: some View {
        ProfilView(state: $viewModel.state,
                   isMe: id == nil,
                   onLogoutClicked: {
            Task {
                do {
                    try await viewModel.logout()
                } catch {
                    
                }
            }
        })
        .task {
            await fetchData()
        }
    }
}

extension ProfilRoute {
    func fetchData() async {
        do {
            try await Task.sleep(nanoseconds: MainRepository.shared.serverDelay)
            Task {
                do {
                    print("Task fetchProfilData: \(id)")
                    let data = try await viewModel.fetchProfilData(id: id)
                    viewModel.updateState(profil: data, creator: nil)
                }catch {
                    viewModel.state = .error("Error info: \(error)")
                }
            }
            
            Task {
                do {
                    print("Task fetchCreatorData: \(id)")
                    let creatorData = try await viewModel.fetchCreatorData(id: id)
                    viewModel.updateState(profil: nil, creator: creatorData)
                }catch {
                    viewModel.state = .error("Error info: \(error)")
                }
            }
            
        } catch {
            viewModel.state = .error("Error info: \(error)")
        }
    }
}
