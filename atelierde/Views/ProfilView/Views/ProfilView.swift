//
//  ProfilView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 31/07/2023.
//
//  

import SwiftUI

struct ProfilView: View {
    
    @Binding var state : ProfilState
    var isMe : Bool
    
    let onLogoutClicked : () -> ()
    
    @State private var showingSheet = false
    
    var body: some View {
        VStack {
            switch state {
            case .loading:
                loadingView
                
            case .success (let user):
                ScrollView(showsIndicators: false) {
                    HStack{
                        Spacer()
                        if isMe {
                            menuContent
                        }
                    }
                    
                    Text(user.name)
                        .modifier(TitleStyle())
                    Text("\(user.experience) année de couture")
                        .modifier(TextSecondaryStyle())
                    Divider()
                    
                    if let first = user.creationsList.first {
                        gridContent(postGrid: user)
                    } else {
                        Text("n'a pas de création")
                    }
                    
                    
                }
                .padding(.horizontal)
                
            case .error (let error):
                Text(error ?? "Vous etes perdu ?")
            }
        }
    }
    
    var menuContent: some View {
        Menu {
            Button(){
                
            } label: {
                Text("profil.menuEdit")
            }
            Button(){
                onLogoutClicked()
            } label: {
                Text("profil.menuOff")
            }
        } label: {
            Image(systemName: "rectangle.and.pencil.and.ellipsis.rtl")
        }
    }
    
    
    func gridContent(postGrid: ProfilState.User) -> some View {
        LazyVGrid(
            columns: [
                GridItem(.adaptive(minimum: 120)),
                GridItem(.adaptive(minimum: 120)),
                GridItem(.adaptive(minimum: 120))
            ], spacing: 10) {
                ForEach(postGrid.creationsList, id: \.id) { post in
                    Button() {
                        showingSheet.toggle()
                    } label: {
                        AsyncImage(url: URL(string: post.image!)) { image in
                            image
                                .ImageStyle()
                        } placeholder: {
                            Color.gray
                                .EmptyImageLoading()
                                .modifier(ImageLittleSize())
                        }
                    }
                    .sheet(isPresented: $showingSheet) {
                        PostDetailsRoute(id: post.id!, showCreator: false)
                    }
                }
            }
    }
}


struct ProfilView_Previews: PreviewProvider {
    static var previews: some View {
        
        let profilPreview = ProfilState.User(name: "Tutu", experience: 7, creationsList: [
            ProfilPost(id: 4, image: "")
        ])
        
        ProfilView(
            state: .constant(.success(profilPreview)),
            isMe: true,
            onLogoutClicked: {})
        
        ProfilView(state: .constant(.loading),
                   isMe: false,
                   onLogoutClicked: {})
        
        ProfilView(state: .constant(.error()),
                   isMe: false,
                   onLogoutClicked: {})
    }
}
