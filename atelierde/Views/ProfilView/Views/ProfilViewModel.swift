//
//  ProfilViewModel.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 14/08/2023.
//
//  

import Foundation

class ProfilViewModel: ObservableObject {
    
    @Published var state : ProfilState
    
    private let mainRepository = MainRepository.shared
    
    private var profilResponse : UserResponse? = nil
    private var creatorResponse : [CreatorResponse]? = nil
    
    init(state: ProfilState = .loading) {
        self.state = state
    }
    
    
    func fetchProfilData(id: Int?) async throws -> UserResponse {
        return try await mainRepository.userData(id: id)
    }
    
    func fetchCreatorData(id: Int?) async throws -> [CreatorResponse] {
        return try await mainRepository.creatorData(id: id)
    }
    
    func updateState(profil : UserResponse?, creator : [CreatorResponse]?) {
        
        if let profil {
            profilResponse = profil
        }

        if let creator {
            creatorResponse = creator
        }
        
        if let profilResponse, let creatorResponse {
            state = toState(profilResponse: profilResponse, creatorResponse: creatorResponse)
        }
    }
    
    func toState(profilResponse : UserResponse, creatorResponse : [CreatorResponse]) -> ProfilState {
        
        guard let experience = profilResponse.experience else {
            return .error()
        }
            
        let post = creatorResponse.map { creation in
            ProfilPost(
                id:creation.id ,
                image: creation.images.first!)
        }
        
        let profilState = ProfilState.success(
            ProfilState.User(
                name: profilResponse.name,
                experience: experience,
                creationsList: (post))
        )
        
        return profilState
    }

    
    func logout() async throws {
        try await mainRepository.logout()
    }
}
