//
//  PatternDetailsRoute.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 10/07/2023.
//
//

import SwiftUI

struct PatternDetailsRoute: View {
    
    var id : Int
    @ObservedObject var viewModel = PatternDetailsViewModel()
    
    var body: some View {
        PatternDetailsView(id: id, state: $viewModel.state)
            .task {
                await fetchData()
            }
    }
}

extension PatternDetailsRoute {
    func fetchData() async {
        do {
            let data = try await viewModel.fetchPatternDetailsData(id: id)
            let state = viewModel.toState(patternDetailsResponse: data)
            
            try await Task.sleep(nanoseconds: MainRepository.shared.serverDelay)
            viewModel.state = state
            // Stop loader, success
        } catch {
            print("Error info: \(error)")
        }
        
    }
}
