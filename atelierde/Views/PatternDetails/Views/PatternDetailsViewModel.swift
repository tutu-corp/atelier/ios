//
//  PatternDetailsViewModel.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 12/07/2023.
//
//  

import Foundation

class PatternDetailsViewModel: ObservableObject {
    
    @Published var state : PatternDetailsState
    
    private let mainRepository = MainRepository.shared
    
    init(state: PatternDetailsState = .loading) {
        self.state = state
    }
    
    func fetchPatternDetailsData(id: Int) async throws -> PatternDetailsResponse {
        return try await mainRepository.patternData(id: id)
    }
    
    func toState(patternDetailsResponse : PatternDetailsResponse) -> PatternDetailsState {

        let patternDetailsState = PatternDetailsState.success(
            PatternDetailsState.Pattern(
                id: patternDetailsResponse.id,
                name: patternDetailsResponse.name,
                creatorName: patternDetailsResponse.creatorName,
                sizes: patternDetailsResponse.sizes,
                images: [patternDetailsResponse.filePath],
                targets: patternDetailsResponse.targets
            )
        )
        
        return patternDetailsState
    }
}

