//
//  PatternDetailsView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 10/07/2023.
//
//  

import SwiftUI

struct PatternDetailsView: View {
    
    var id : Int
    @Binding var state : PatternDetailsState
    
    var idPostGrid : [Int] = [1,2,3,4,5,6,7,8,9,10]
    
    var body: some View {
        VStack {
            switch state {
            case .loading:
                
                loadingView
                
            case .success (let pattern):
                ScrollView(showsIndicators: false) {
                    LazyVStack{
                        SliderView(images: pattern.images)
                        
                        
                        VStack(alignment: .leading) {
                            
                            titleContent(pattern.name)
                            largeSpacer
                            
                            detailsContent(creator: pattern.creatorName, targets: pattern.targets, sizes: pattern.sizes)
                            largeSpacer
                            postGrid
                            
                        }
                        .padding()
                    }
                }
                
            case .error:
                Text("Vous etes perdu ?")
            }
        }
    }
    
    private func titleContent(_ name : String) -> some View {
        Text(name)
            .modifier(TitleStyle())
    }
    
    func detailsContent(creator : String, targets : [String], sizes : [String]) -> some View {
        Group {
            HStack{
                Text("patternDetails.creator")
                    .modifier(TextSecondaryStyle())
                Spacer()
                Text(creator)
            }
            
            Divider()
            
            HStack{
                Text("patternDetails.targets")
                    .modifier(TextSecondaryStyle())
                Spacer()
                
                Text(targets.joined(separator: ", "))
            }
            
            Divider()
            
            HStack{
                Text("patternDetails.sizes")
                    .modifier(TextSecondaryStyle())
                Spacer()
                Text(sizes.joined(separator: ", "))
            }
        }
    }
    
    var largeSpacer : some View {
        Spacer()
            .frame(height: 35)
    }
    
    var postGrid : some View {
        LazyVGrid(
            columns: [
                GridItem(.adaptive(minimum: 120)),
                GridItem(.adaptive(minimum: 120)),
                GridItem(.adaptive(minimum: 120))
            ], spacing: 10) {
                ForEach(idPostGrid, id: \.self) { post in
                    NavigationLink {
                        PostDetailsRoute(id: 1)
                    } label: {
                        Image("crea-bob1")
                            .ImageStyle()
                    }
                }
            }
    }
}

struct PatternDetailsViewSuccess_Previews: PreviewProvider {
    static var previews: some View {
        
        let patternPreview = PatternDetailsState.Pattern(id: 3, name: "Nom du Patron", creatorName: "Nom du créateur", sizes: ["44","42"], images: [], targets: ["Homme"])
        
        PatternDetailsView(id: 1, state: .constant(.success(patternPreview)))
        
        PatternDetailsView(id: 1, state: .constant(.loading))
        
        PatternDetailsView(id: 1, state: .constant(.error()))
    }
}
