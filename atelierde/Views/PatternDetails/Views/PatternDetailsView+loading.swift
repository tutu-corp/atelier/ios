//
//  PatternDetailsView+loading.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 04/08/2023.
//
//  

import Foundation
import Shimmer
import SwiftUI

extension PatternDetailsView {
    var loadingView : some View {
        ScrollView(showsIndicators: false) {
            
            let patternLoading = PatternDetailsState.Pattern(id: 3, name: "Nom du Patron", creatorName: "Nom du créateur", sizes: ["44","42"], images: [], targets: ["Homme"])
            
            VStack{
                
                Color.gray
                    .EmptyImageLoading()
                
                largeSpacer
                detailsContent(creator: patternLoading.creatorName, targets: patternLoading.targets, sizes: patternLoading.sizes)
                largeSpacer
                postGrid
            }
        }
        .padding()
        .redacted(reason: .placeholder)
        .shimmering()
    }
}
