//
//  PatternDetailsState.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 12/07/2023.
//
//  

import Foundation

enum PatternDetailsState: Equatable {
    
    struct Pattern: Codable, Equatable {
        var id: Int
        var name: String
        var creatorName: String
        var sizes : [String]
        var images : [String]
        var targets: [String]
    }
    
    case loading
    case success(Pattern)
    case error(String? = nil)
}
