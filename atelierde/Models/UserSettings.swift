//
//  UserLogged.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 25/07/2023.
//
//  

import Foundation

class User: ObservableObject {
    
    @Published private(set) var token : String?

    var isLoggedIn: Bool {
        token != nil
    }
    
    init(token: String?) {
        self.token = token
    }
    
    func setToken(_ token: String?) {
        self.token = token
        UserDefaults.standard.set(token, forKey: "TokenBearer")
    }
    
}




