//
//  PatternDetailsResponse.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 12/07/2023.
//
//  

import Foundation

struct PatternDetailsResponse : Codable {
    var id: Int
    var name: String
    var creatorName: String
    var filePath: String
    var sizes : [String]
    var targets: [String]
}
