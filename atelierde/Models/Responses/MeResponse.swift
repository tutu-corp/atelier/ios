//
//  MeResponse.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 21/08/2023.
//
//  

import Foundation

struct MeResponse {
    var id: Int
}
