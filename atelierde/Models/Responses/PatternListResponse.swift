//
//  PatternsListResponse.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 10/07/2023.
//
//  

import Foundation


struct PatternListResponse : Codable {
    var id: Int
    var name: String
    var creatorName: String
    var filePath : String
}

