//
//  Post.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 20/06/2023.
//
//  

import Foundation

struct HomeResponse : Codable {
    let creations: [CreationResponse]
    
    struct CreationResponse : Codable {
        var id: Int
        var user: User
        var images: [String]
        
        struct User : Codable {
            var id: Int
            var name: String
        }
    }
}




