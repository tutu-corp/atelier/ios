//
//  CreatorResponse.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 18/08/2023.
//
//  

import Foundation

struct CreatorResponse : Codable {
        var id: Int
        var images: [String]
}

