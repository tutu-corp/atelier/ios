//
//  UserResponse.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 14/08/2023.
//
//  

import Foundation

struct UserResponse : Codable {
    var name : String
    var experience : Int?
}
