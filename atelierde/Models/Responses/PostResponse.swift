//
//  PostResponse.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 27/06/2023.
//
//  

import Foundation

struct PostResponse : Codable {
    var id: Int?
    var patternName: String?
    var creationDate: String?
    var user: User?
    var size : SizeResponse?
    var images: [String]?
    var fabrics: [String]?
    
    struct User : Codable {
        var id: Int?
        var name: String?
        var exp: Int?
    }
    
    struct SizeResponse : Codable {
        var code: String?
        var type: String?
    }
}
