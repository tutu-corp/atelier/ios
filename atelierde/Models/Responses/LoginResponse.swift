//
//  LoginResponse.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 21/07/2023.
//
//  

import Foundation

struct LoginResponse {
    var email: String
    var fullName: String
}
