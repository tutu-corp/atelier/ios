//
//  DefaultMainRepository.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 20/06/2023.
//
//  

import Foundation

class DefaultMainRepository : MainRepositoryAPI {

    private let dataSource = RemoteDataSource.shared
    private let downloader = URLSession.shared
    private let decoder = Decoder.shared.decoder
    
    var user : User = User(token:UserDefaults.standard.string(forKey: "TokenBearer"))
    private var token: String{
        user.token!
    }
    
    var serverDelay: UInt64 = 0
    
    // return list of all post
    func homeData() async throws -> HomeResponse {
        var request = URLRequest(url: dataSource.home)
        request.setValue(token, forHTTPHeaderField: "Authorization")
        
        let data = try await downloader.data(for: request).0
        print("homeData() " + String(decoding: data, as: UTF8.self))
        let decodedData = try decoder.decode(HomeResponse.self, from: data)
        
        return decodedData
    }
    
    func patternListData() async throws -> [PatternListResponse] {
        var request = URLRequest(url: dataSource.patternsList)
        request.setValue(token, forHTTPHeaderField: "Authorization")
        let data = try await downloader.data(for: request).0
        print("patternListData() " + String(decoding: data, as: UTF8.self))
        let decodedData = try decoder.decode([PatternListResponse].self, from: data)
        
        return decodedData
    }
    
    
    func login(email: String) async throws -> String {
        let json: [String: Any] = ["email": email]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        var request = URLRequest(url: dataSource.login)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let data = try await downloader.data(for: request).0
        let decodedData = String(decoding: data, as: UTF8.self)
        
        print("login(email:\(email)) " + decodedData)
        return decodedData
    }
    
    // Return post by id
    func postData(id: Int) async throws -> PostResponse {
        var request = URLRequest(url: dataSource.post(id: id))
        request.setValue(token, forHTTPHeaderField: "Authorization")
        let data = try await downloader.data(for: request).0
        print("postData(id:\(id)) " + String(decoding: data, as: UTF8.self))
        let decodedData = try decoder.decode(PostResponse.self, from: data)
        
        return decodedData
    }
    
    // Return pattern by id
    func patternData(id: Int) async throws -> PatternDetailsResponse {
        var request = URLRequest(url: dataSource.pattern(id: id))
        request.setValue(token, forHTTPHeaderField: "Authorization")
        let data = try await downloader.data(for: request).0
        print("patternData(id:\(id)) " + String(decoding: data, as: UTF8.self))
        let decodedData = try decoder.decode(PatternDetailsResponse.self, from: data)
        
        return decodedData
    }
    
    // Return user by id
    func userData(id: Int? = nil) async throws -> UserResponse {
        
        var request: URLRequest
        
        if let id = id {
            request = URLRequest(url: dataSource.user(id: id))
        } else {
            request = URLRequest(url: dataSource.me())
        }
        
        request.setValue(token, forHTTPHeaderField: "Authorization")
        let data = try await downloader.data(for: request).0
        print("userData(id:\(String(describing: id))) " + String(decoding: data, as: UTF8.self))
        let decodedData = try decoder.decode(UserResponse.self, from: data)
        
        return decodedData
    }
    
    func creatorData(id: Int?) async throws -> [CreatorResponse] {
        
        var request = URLRequest(url: dataSource.creationsByCreator(id: id))
        
        request.setValue(token, forHTTPHeaderField: "Authorization")
        let response = try await downloader.data(for: request)
        
        if let httpResponse = response.1 as? HTTPURLResponse {
            print("statusCode: \(httpResponse.statusCode)")
            
            switch httpResponse.statusCode {
            case 200:
                let data = response.0
                
                print("creatorData(id:\(String(describing: id))) " + String(decoding: data, as: UTF8.self))
                let decodedData = try decoder.decode([CreatorResponse].self, from: data)
                
                return decodedData
            case 404:
                return []
            default:
                throw ServerError.io("Error httpResponse")
            }
        }
            throw ServerError.io("Error requestURL")
    }
    
    func logout() {
        user.setToken(nil)
    }
}

enum ServerError: Error {
    case io(String)
}
