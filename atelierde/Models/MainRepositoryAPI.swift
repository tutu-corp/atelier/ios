//
//  MainRepositoryAPI.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 14/08/2023.
//
//  

import Foundation

protocol MainRepositoryAPI {
    
    var user : User { get }
    
    var serverDelay: UInt64 { get }
    
    // return list of all post
    func homeData() async throws -> HomeResponse
    
    // return list of all patterns
    func patternListData() async throws -> [PatternListResponse]
    
    func login(email: String) async throws -> String
    
    // Return post by id
    func postData(id: Int) async throws -> PostResponse
    
    // Return pattern by id
    func patternData(id: Int) async throws -> PatternDetailsResponse
    
    // Return user by id
    func userData(id: Int?) async throws -> UserResponse
    
    // Return all post of creator by id creator
    func creatorData(id: Int?) async throws -> [CreatorResponse]

    // log out user
    func logout() async throws
}

class MainRepository {
    public static let shared: MainRepositoryAPI = DefaultMainRepository()
//    public static let shared: MainRepositoryAPI = FakeMainRepository()
    
    private init() {
        
    }
}
