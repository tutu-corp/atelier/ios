//
//  RemoteDataSource.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 20/06/2023.
//
//  

import Foundation

class RemoteDataSource {
    
    public static let shared = RemoteDataSource()
    
    let host = "http://127.0.0.1:8080/"
    
    let login: URL
    let home: URL
    let patternsList: URL
    
    private init() {
        self.login = URL(string: host + "login")!
        self.home = URL(string: host)!
        self.patternsList = URL(string: host + "patterns")!
    }
    
    func post(id: Int) -> URL {
        print("post id: \(id)")
        return URL(string: host + "creations/\(id)")!
    }
    
    func pattern(id: Int) -> URL {
        print("pattern id: \(id)")
        return URL(string: host + "patterns/\(id)")!
    }
    
    func user(id:Int) -> URL {
        print("user id: \(id)")
        return URL(string:host + "users/\(id)")!
    }
    
    func creationsByCreator(id:Int?) -> URL {
        print("creationsByCreator id: \(id)")
        
        if let id {
            return URL(string:host + "users/\(id)/creations")!
        } else {
            return URL(string:host + "users/me/creations")!
        }
        
        return URL(string:host + "users/\(id)/creations")!
    }
    
    func me() -> URL {
        return URL(string:host + "users/me")!
    }

}
