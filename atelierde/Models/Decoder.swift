//
//  Decoder.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 20/06/2023.
//
//  

import Foundation

class Decoder {
    
    public static let shared = Decoder()
    
    var decoder: JSONDecoder = {
        let aDecoder = JSONDecoder()
        return aDecoder
    }()
    
    private init(){
        
    }
}
