//
//  FakeMainRepository.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 14/08/2023.
//
//  

import Foundation

class FakeMainRepository: MainRepositoryAPI {
    
    var user : User = User(token:UserDefaults.standard.string(forKey: "TokenBearer"))
    
    var serverDelay: UInt64 = 1_000_000_000
    
    func homeData() async throws -> HomeResponse {
        return HomeResponse(creations: [
            
            // Short 1
            HomeResponse.CreationResponse(
                id: 1,
                user: HomeResponse.CreationResponse.User(
                    id: 2,
                    name: "Balou"
                ),
                images: ["https://cdn.discordapp.com/attachments/1053259778597601342/1141721850888585286/IMG_4483.jpg"]),
            
            // Chemise 1
            HomeResponse.CreationResponse(
                id: 2,
                user: HomeResponse.CreationResponse.User(
                    id: 8,
                    name: "Madame Quiche"
                ),
                images: ["https://cdn.discordapp.com/attachments/1053259778597601342/1141722150689062962/IMG_4456.jpg"]),
            
            // Short 2
            HomeResponse.CreationResponse(
                id: 3,
                user: HomeResponse.CreationResponse.User(
                    id: 2,
                    name: "Balou"
                ),
                images: ["https://cdn.discordapp.com/attachments/1053259778597601342/1124994353463574548/IMG_4485.jpg"]),
            
            // Top espace
            HomeResponse.CreationResponse(
                id: 4,
                user: HomeResponse.CreationResponse.User(
                    id: 2,
                    name: "Balou"
                ),
                images: ["https://cdn.discordapp.com/attachments/1053259778597601342/1141721897734774814/IMG_4486.jpg"]),
            
            // Veste 1
            HomeResponse.CreationResponse(
                id: 5,
                user: HomeResponse.CreationResponse.User(
                    id: 8,
                    name: "Madame Quiche"
                ),
                images: ["https://cdn.discordapp.com/attachments/1053259778597601342/1141725587078131722/IMG_4080.jpg"]),
        ])
    }
    
    func patternListData() async throws -> [PatternListResponse] {
        return [
            PatternListResponse(
                id: 11,
                name: "Le Sarouel Short",
                creatorName: "Aladdin",
                filePath: "https://cdn.discordapp.com/attachments/1053259778597601342/1141728304899047544/IMG_4482.jpg"),
            PatternListResponse(
                id: 12,
                name: "Chemise Froufrou",
                creatorName: "Bigoudi le sacré",
                filePath: "https://cdn.discordapp.com/attachments/1053259778597601342/1141728025222852688/IMG_4456.jpg"),
            PatternListResponse(
                id: 13,
                name: "Veste gindée",
                creatorName: "Georgette",
                filePath: "https://cdn.discordapp.com/attachments/1053259778597601342/1141728471966548079/patron-veste.png")
        ]
    }
    
    func login(email: String) async throws -> String {
        return ""
    }
    
    func postData(id: Int) async throws -> PostResponse {
        
        switch id {
        case 1:
            return PostResponse(
                id: 1,
                patternName: "Le Sarouel short",
                creationDate: "2023",
                user: PostResponse.User(
                    id: 2,
                    name: "Balou",
                    exp: 1
                ),
                size: PostResponse.SizeResponse(
                    code: "24 mois",
                    type: "Enfant"
                ),
                images: ["https://cdn.discordapp.com/attachments/1053259778597601342/1141721850888585286/IMG_4483.jpg",
                         "https://cdn.discordapp.com/attachments/1053259778597601342/1141721851408687135/IMG_4484.jpg"],
                fabrics: ["Jersey"]
            )
        case 2:
            return PostResponse(
                id: 2,
                patternName: "Chemise Froufrou",
                creationDate: "2023",
                user: PostResponse.User(
                    id: 8,
                    name: "Madame Quiche",
                    exp: 5
                ),
                size: PostResponse.SizeResponse(
                    code: "50 ans",
                    type: "Femme"
                ),
                images: ["https://cdn.discordapp.com/attachments/1053259778597601342/1141722150689062962/IMG_4456.jpg",
                         "https://cdn.discordapp.com/attachments/1053259778597601342/1141722151142051910/IMG_4455.jpg"],
                fabrics: ["Poupeline", "Viscose"]
            )
        case 3:
            return PostResponse(
                id: 4,
                patternName: "Le Sarouel",
                creationDate: "2023",
                user: PostResponse.User(
                    id: 2,
                    name: "Balou",
                    exp: 1
                ),
                size: PostResponse.SizeResponse(
                    code: "2 ans",
                    type: "Enfant"
                ),
                images: ["https://cdn.discordapp.com/attachments/1053259778597601342/1124994353463574548/IMG_4485.jpg"],
                fabrics: ["Jersey"]
            )
        case 4:
            return PostResponse(
                id: 5,
                patternName: "Le t-shirt",
                creationDate: "2023",
                user: PostResponse.User(
                    id: 2,
                    name: "Balou",
                    exp: 1
                ),
                size: PostResponse.SizeResponse(
                    code: "2 ans",
                    type: "Enfant"
                ),
                images: ["https://cdn.discordapp.com/attachments/1053259778597601342/1124994352981225572/IMG_4486.jpg",
                         "https://cdn.discordapp.com/attachments/1053259778597601342/1124994351156703282/IMG_4487.jpg"],
                fabrics: ["Jersey"]
            )
        default:
            return PostResponse(
                id: 3,
                patternName: "Veste",
                creationDate: "2023",
                user: PostResponse.User(
                    id: 8,
                    name: "Madame Quiche",
                    exp: 5
                ),
                size: PostResponse.SizeResponse(
                    code: "48",
                    type: "Femme"
                ),
                images: ["https://cdn.discordapp.com/attachments/1053259778597601342/1141725588596473856/IMG_4089.jpg",
                         "https://cdn.discordapp.com/attachments/1053259778597601342/1141722151142051910/IMG_4455.jpg"],
                fabrics: ["Poupeline", "Viscose", "Coton"]
            )
        }
    }
    
    func patternData(id: Int) async throws -> PatternDetailsResponse {
        
        switch id {
        case 11:
            return PatternDetailsResponse(
                id: 11,
                name: "Le Sarouel Short",
                creatorName: "Aladdin",
                filePath: "https://cdn.discordapp.com/attachments/1053259778597601342/1141728304899047544/IMG_4482.jpg",
                sizes: ["6 mois", "8 mois", "10 mois", "12 mois", "18 mois", "24 mois"],
                targets: ["Enfant"]
            )
        case 12:
            return PatternDetailsResponse(
                id: 12,
                name: "Chemise Froufrou",
                creatorName: "Bigoudi le sacré",
                filePath: "https://cdn.discordapp.com/attachments/1053259778597601342/1141728025222852688/IMG_4456.jpg",
                sizes: ["34 à 60"],
                targets: ["Femme"]
            )
        default:
            return PatternDetailsResponse(
                id: 12,
                name: "Veste gindée",
                creatorName: "Georgette",
                filePath: "https://cdn.discordapp.com/attachments/1053259778597601342/1141728471966548079/patron-veste.png",
                sizes: ["de 34 à 60"],
                targets: ["Femme"]
                
                
            )
        }
        
    }
    
    func userData(id: Int?) async throws -> UserResponse {
        switch id {
        case 2 :
            return UserResponse(
                name: "Balou",
                experience: 1
            )
        case 8 :
            return UserResponse(
                name: "Madame Quiche",
                experience: 5
            )
        default:
            return UserResponse(
                name: "Tapioka",
                experience: 7
            )
        }
    }
    
    func creatorData(id: Int?) async throws -> [CreatorResponse] {
        switch id {
        case nil :
            return []
        default:
            return [
                CreatorResponse(
                    id: 11,
                    images: ["https://cdn.discordapp.com/attachments/1053259778597601342/1141728304899047544/IMG_4482.jpg"])
            ]
        }
    }
    
    func logout() {
        user.setToken(nil)
    }
    
    
}
