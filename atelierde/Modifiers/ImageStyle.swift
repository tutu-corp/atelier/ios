//
//  ImageStyle.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 15/06/2023.
//
//  

import SwiftUI

extension Image {
    func ImageStyle() -> some View {
        self
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(minWidth: 100, maxWidth: .infinity, minHeight: 100, maxHeight: .infinity)
            .clipShape(RoundedRectangle(cornerRadius: 8, style: .continuous))
            .aspectRatio(1, contentMode: .fit)
   }
}
// roundedSquare

extension Color {
    func EmptyImageLoading() -> some View {
        return self
            .aspectRatio(contentMode: .fill)
            .frame(minWidth: 100, maxWidth: 250, minHeight: 100, maxHeight: 250)
            .clipShape(RoundedRectangle(cornerRadius: 8, style: .continuous))
            .aspectRatio(1, contentMode: .fit)
   }
}

struct ImageLittleSize: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(maxWidth: 100)
    }
}

// LittleSize

struct ImageView_Previews: PreviewProvider {
    static var previews: some View {
        Image("crea-bob1")
            .ImageStyle()
            .modifier(ImageLittleSize())
        
        Image("crea-bob1")
            .ImageStyle()
    }
    
}


