//
//  TextStyle.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 26/06/2023.
//
//  

import SwiftUI

struct TextSecondaryStyle: ViewModifier {
        func body(content: Content) -> some View {
            content
                .foregroundColor(.gray)
        }
    }

struct TitleStyle: ViewModifier {
        func body(content: Content) -> some View {
            content
                .foregroundColor(.primary)
                .font(.custom("Mangabey", size: 46))
        }
    }






// Previews

struct TextStyleSecondary_Previews: PreviewProvider {
    static var previews: some View {
        HStack {
                Image(systemName: "star")
                Text("Le Texte")
            }
            .modifier(TextSecondaryStyle())
    }
}

struct TextStyleTitle_Previews: PreviewProvider {
    static var previews: some View {
        HStack {
                Text("Le Texte")
            }
            .modifier(TitleStyle())
    }
}
