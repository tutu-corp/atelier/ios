//
//  CardStyle.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 26/06/2023.
//
//  

import SwiftUI

struct CardStyle: ViewModifier {
    func body(content: Content) -> some View {
           content
            .padding()
               .background(.white)
               .cornerRadius(12)
               .shadow(color: Color(.systemGray4), radius: 8)
       }
    }

struct CardStyle_Previews: PreviewProvider {
    static var previews: some View {
        HStack {
                Image(systemName: "star")
                Text("Le Texte")
            }
            .modifier(CardStyle())
    }
}

