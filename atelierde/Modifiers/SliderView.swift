//
//  SliderView.swift of project named atelierde
//
//  Created by Aurore D (@Tuwleep) on 26/06/2023.
//
//  

import SwiftUI

struct SliderView: View {
    
    let images: [String]
    
    var body: some View {
        ZStack{
            TabView(){
                ForEach(images, id: \.self){ i in
                    AsyncImage(url: URL(string: i)) { image in
                            image
                                .ImageStyle()
                        } placeholder: {
                        }
                }
            }
            .tabViewStyle(PageTabViewStyle())
            .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
        }
        .frame(minWidth: 280,minHeight: 280)
    }
}

struct SliderView_Previews: PreviewProvider {
    static var previews: some View {
        SliderView(images: ["images/creations/4b95ae7a-40f9-407d-bded-ed463d5ee582.jpeg"])
    }
}
